# step-project-tinder
MVP додатoк для знайомств Tinder.
#### Над проектом працювали:

#### Vlad Baryshpolets

#### Olena Mits

#### Serhii Tyshchenko

Для доступу до бази даних були розроблені класи(далі сервіс-контролери) які реалізують відповідні інтерфейси ДАО, 
вони містять два типи методів одні без прямого доступу - приватні виступають як сервіс,
інші це контролери що забезпечують доступ до цих методів сервісу.

- [проект розгорнутий на хероку](http://stinder.herokuapp.com/)
- База даних MySQL розташована на [ Ukraine.com.ua ](https://www.ukraine.com.ua/)
- Проект розроблений для роботи з PostgreSql та MySql
- у файлі міграції `V0001__initial_schema.sql` описано два відповідні діалекти SQL для створення схеми ДБ

Список ендпоінтів:

- `/`
- `/login`
- `/logout`
- `/sign-up`
- `/users`
- `/messages/{id}`
- `/liked`

### Olena Mits працювала над функціоналом лайків:

- Створення сервіс-контролера БД: 
- [ ] `LikesInDB`
- [ ] Створення сервлетів:
- [ ] `UsersServlet`
- [ ] `LikedServlet`

- Розроблені темплейти:

- [ ] `people-list.ftl`
- [ ] `like-page.ftl`


### Vlad Baryshpolets працював над функціоналом авторизації, фільтрації та
### та рефакторингом інтерфейсів сервіс-контролерів БД:

- [ ] Створення сервлетів:
- [ ] `SignInServlet`
- [ ] `SignUpServlet`
- [ ] `StaticContentServlet`

- Розроблені інтерфейси:

- [ ] `UserDAO`
- [ ] `MessagesDAO`
- [ ] `LikesDAO`
- [ ] `HttpFilter`
- [ ] `ChatsDAO`

### Serhii Tyshchenko працював над функціоналом чату:

- Створення сервіс-контролера БД:
- [ ] `ChatsInDb`
- [ ] `MessagesInDb`
- [ ] Створення сервлетів:
- [ ] `ChatServlet`

- Розроблені темплейти:

- [ ] `macros.ftl`
- [ ] `tplMenu.ftl`
- [ ] `home.ftl`

Розробка моделі, сервіс-контролера БД `UsersInDb` т.ін. проводилася спільно в онлайн режимі.
