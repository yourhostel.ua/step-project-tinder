<div>
    <h1 class="h3 mb-3 font-weight-normal text-sm-center text-white">Please sign up</h1>

    <label for="loginFirstName" class="sr-only" >First Name</label>
    <input type="text" id="loginFirstName" name="firstname" class="form-control" placeholder="Enter first name" required autofocus>


    <label for="loginLastName" class="sr-only" >First Name</label>
    <input type="text" id="loginLastName" name="lastname" class="form-control" placeholder="Enter last name" required autofocus>

    <label for="positionRole" class="sr-only" >Position role</label>
    <input type="text" id="positionRole" name="role" class="form-control" placeholder="Enter position role" required autofocus>


    <label for="loginInput" class="sr-only" >login</label>
    <input type="text" id="loginInput" name="login" class="form-control" placeholder="Enter login" required autofocus>

    <label for="imgInput" class="sr-only" >Img</label>
    <input type="text" id="imgInput" name="img" class="form-control" placeholder="attach link for your img" required autofocus>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    <p class="mt-5 mb-3 text-white text-justify text-sm-center">&copy; Tinder 2018</p>

</div>
