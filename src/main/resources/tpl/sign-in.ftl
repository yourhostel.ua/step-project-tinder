<div>
    <h1 class="h3 mb-3 font-weight-normal text-white text-sm-center">Sign in</h1>

    <label for="loginInput" class="sr-only">Login</label>
    <input type="text" id="loginInput" name="login" class="form-control" placeholder="Enter login" required autofocus>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
    <p class="mt-5 mb-3 text-white text-justify text-sm-center">&copy; Tinder 2018</p>
</div>
