<#import "macros.ftl" as ui/>
<@ui.homeTop buttonA="${buttonA}" buttonB="${buttonB}" tplMenu="${tplMenu}"/>
        <div class="row">
            <div class="col-8 offset-2">
                <div class="panel panel-default user_panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">User List</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <table class="table-users table" border="0">
                                <tbody>
                                    <#list likes as l>
                                        <tr onclick="window.location='/messages/${l.id()}';">
                                            <input name="idTo" value=${l.id()} class="d-none">
                                            <td width="10">
                                                    <div class="avatar-img">
                                                        <img class="img-circle" src=${l.img()} />
                                                    </div>
                                            </td>
                                            <td class="align-middle">
                                                    ${l.firstname()} ${l.lastname()}
                                            </td>
                                            <td class="align-middle">
                                                ${l.role()}
                                            </td>
                                            <td  class="align-middle">
                                                Last Login:  ${l.getLastLogin()}<br><small class="text-muted">${l.getDaysOfLogin()}</small>
                                            </td>
                                        </tr>
                                    </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<@ui.homeBottom/>
