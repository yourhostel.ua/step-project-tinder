<#import "macros.ftl" as ui/>
<@ui.homeTop buttonA="${buttonA}" buttonB="${buttonB}" tplMenu="${tplMenu}"/>
    <div class="col-4 offset-4">
        <#list users as u>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-lg-12 col-md-12 text-center">
                            <img src=${u.img()} alt="" class="mx-auto rounded-circle img-fluid">
                            <h3 class="mb-0 text-truncated">${u.firstname()} ${u.lastname()}</h3>
                            <br>
                        </div>
                        <div class="col-12 col-lg-6">
                            <form method="post">
                                <input name="idTo" value=${u.id()} class="d-none">
                                <button id="dislikeBtn" type="submit" name="like" value="dislike" class="btn btn-outline-danger btn-block"><span class="fa fa-times"></span> Dislike</button>
                            <form>
                        </div>
                        <div class="col-12 col-lg-6">
                            <form method="post">
                                <input name="idTo" value=${u.id()} class="d-none">
                                <button id="likeBtn" type="submit" name="like" value="like" class="btn btn-outline-success btn-block"><span class="fa fa-heart"></span> Like</button>
                            <form>
                        </div>
                        <!--/col-->
                    </div>
                    <!--/row-->
                </div>
                <!--/card-block-->
            </div>
        </#list>
    </div>
<@ui.homeBottom/>