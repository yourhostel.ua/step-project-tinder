<#import "macros.ftl" as ui/>
<@ui.homeTop buttonA="${buttonA}" buttonB="${buttonB}" tplMenu="${tplMenu}"/>
    <div class="row">
        <div class="chat-main col-6 offset-3">
            <div class="col-md-12 chat-header">
                <div class="row header-one text-white p-1">
                    <div class="col-md-6 name pl-2">
                        <i class="fa fa-comment"></i>
                        <h6 class="ml-1 mb-0">${convUser}</h6>
                    </div>
                    <div class="col-md-6 options text-right pr-0">
                        <i class="fa fa-window-minimize hide-chat-box hover text-center pt-1"></i>
                        <p class="arrow-up mb-0">
                            <i class="fa fa-arrow-up text-center pt-1"></i>
                        </p>
                        <i class="fa fa-times hover text-center pt-1"></i>
                    </div>
                </div>
                <div class="row header-two w-100">
                    <div class="col-md-6 options-left pl-1">
                        <i class="fa fa-video-camera mr-3"></i>
                        <i class="fa fa-user-plus"></i>
                    </div>
                    <div class="col-md-6 options-right text-right pr-2">
                        <i class="fa fa-cog"></i>
                    </div>
                </div>
            </div>
            <div class="chat-content">
                <div class="col-md-12 chats pt-3 pl-2 pr-3 pb-3 overflow_scroll">
                    <ul class="p-0">
                        <#list items as message>
                        <#if message.text()??>
                        <#if message.user_id() == user_id>
                        <li class="receive-msg float-right mb-2">
                         <div class="sender-img">
                            <img src="${img}" class="float-right">
                        </div>
                        <div class="send-msg float-right ml-2">
                            <p class="bg-info pt-1 pb-1 pl-2 pr-2 m-0 rounded">
                                ${message.text()}
                            </p>
                            <span class="float-right receive-msg-time">${message.date_time()}</span>
                        </div>
                        </li>
                        <#else>
                        <li class="receive-msg float-left mb-2">
                            <div class="sender-img">
                                <img src="${convImg}" class="float-left">
                            </div>
                            <div class="receive-msg-desc float-left ml-2">
                                <p class="bg-warning m-0 pt-1 pb-1 pl-2 pr-2 rounded">
                                    ${message.text()}
                                </p>
                                <span class="receive-msg-time">${convUserFirstname}, ${message.date_time()}</span>
                            </div>
                        </li>
                    </#if>
                    </#if>
                </#list>
                </ul>
            </div>
          <form method="post">
            <div class="col-md-12 p-2 msg-box">
                <div class="row con-send">
                    <div class="col-md-2 options-left">
                        <i class="fa fa-smile-o"></i>
                    </div>
                    <div class="col-md-7 pl-0">
                        <textarea name="text" class="input-message" placeholder=" Send message"/></textarea>
                    </div>
                    <div class="col-md-3 text-right options-right">
                        <button class="float-right send">Send message</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<@ui.homeBottom/>
