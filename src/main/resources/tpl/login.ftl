<#import "macros.ftl" as ui/>
<@ui.homeTop buttonA="${buttonA}" buttonB="${buttonB}" tplMenu="${tplMenu}"/>
<div>
    <form class="form-signin" method="post">
            ${signIn}
            ${signUp}
    </form>
    <div>
        <p class="additional-info">${additionalInfo}</p>
    </div>
</div>
<@ui.homeBottom/>
