--MySQL

create table users
(
    id INT AUTO_INCREMENT NOT NULL UNIQUE,
        CONSTRAINT users_pk
            PRIMARY KEY (id),
    login     varchar(20) NOT NULL UNIQUE,
    firstname varchar(20) NOT NULL,
    lastname  varchar(20) NOT NULL,
    password  varchar(20) NOT NULL,
    img       varchar(100),
    cookie    varchar(36)    NOT NULL
        UNIQUE,
    role  varchar(20)  DEFAULT 'user' NOT NULL,
    last_login TIMESTAMP default CURRENT_TIMESTAMP NOT NULL
);

create table likes
(
    from_id INT,
    to_id   INT,
        CONSTRAINT likes_from_id_fk
        FOREIGN KEY (from_id) REFERENCES users (id),
        CONSTRAINT likes_to_id_fk
        FOREIGN KEY (to_id) REFERENCES users (id)
);

create table chats
(
    id varchar(36) NOT NULL,
        CONSTRAINT chats_pk2
            PRIMARY KEY (id),
        CONSTRAINT chats_pk
            UNIQUE (id)
);

create table chat
(
    id varchar(36) NOT NULL,
        CONSTRAINT chat_chats_id_fk
        FOREIGN KEY (id) REFERENCES chats (id),
    user_id INT NOT NULL,
        CONSTRAINT chat_users_id_fk
        FOREIGN KEY (user_id) REFERENCES users (id),
    text      varchar(1000),
    date_time TIMESTAMP NOT NULL
);

--PostgreSQL

--create table users
--(
--    id        serial
--        constraint users_pk
--            primary key,
--    login     varchar not null
--        constraint users_pk3
--            unique,
--    firstname varchar not null,
--    lastname  varchar not null,
--    password  varchar not null,
--    img       varchar,
--    cookie    varchar(36)    not null
--        unique,
--    role varchar default 'user'::character varying not null,
--    last_login timestamp default CURRENT_TIMESTAMP not null
--);
--
--create table likes
--(
--    from_id integer
--        constraint likes_from_id_fk
--            references users,
--    to_id   integer
--        constraint likes_to_id_fk
--            references users
--);
--
--create table chats
--(
--    id varchar(36) not null
--        constraint chats_pk2
--            primary key
--        constraint chats_pk
--            unique
--);
--
--create table chat
--(
--    id        varchar(36)      not null
--        constraint chat_chats_id_fk
--            references chats,
--    user_id   integer   not null
--        constraint chat_users_id_fk
--            references users,
--    text      varchar,
--    date_time timestamp not null
--);