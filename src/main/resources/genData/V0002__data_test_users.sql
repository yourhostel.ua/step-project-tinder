insert into users (login, firstname, lastname, password, img, cookie)
values  ('j_conn', 'John', 'Connor', '123', 'https://robohash.org/KYC.png?set=set1', '1633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c'),
        ('s_conn', 'Sara', 'Connor', '123', 'https://robohash.org/050.png?set=set1', '1633a3b4-c82f-4ba7-a8f3-bc7b0a66c74c'),
        ('j_francis', 'James', 'Cameron', '123', 'https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg', '1633a3b4-c82f-4ba9-a8f3-bc7b0a66c74c'),
        ('k_reese', 'Kyle', 'Reese', '123', 'https://www.shutterstock.com/image-photo/closeup-portrait-yong-woman-casual-260nw-1554086789.jpg', '1633a3b4-c83f-4ba9-a8f3-bc7b0a66c74c');

insert into chats (id)
values  ('97D253C1-111A-4BDD-8621-B6332DC958D8');

insert into chat (id, user_id, text, date_time)
values  ('97D253C1-111A-4BDD-8621-B6332DC958D8', 1, '"Hii"', '2023-05-17 18:05:54.000000'),
        ('97D253C1-111A-4BDD-8621-B6332DC958D8', 2, '"hiii, How are you ?"', '2023-05-17 18:06:47.000000'),
        ('97D253C1-111A-4BDD-8621-B6332DC958D8', 1, '"nice, Are you fine ? "', '2023-05-17 18:07:40.000000'),
        ('97D253C1-111A-4BDD-8621-B6332DC958D8', 2, '"Yes always"', '2023-05-17 18:08:15.000000'),
        ('97D253C1-111A-4BDD-8621-B6332DC958D8', 1, '"Byy"', '2023-05-17 18:08:48.000000'),
        ('97D253C1-111A-4BDD-8621-B6332DC958D8', 2, '"Byy"', '2023-05-17 18:09:11.000000');