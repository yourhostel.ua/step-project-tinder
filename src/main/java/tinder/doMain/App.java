package tinder.doMain;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import tinder.db.Auth;
import tinder.db.Database;
import tinder.db.sql.ChatsInDb;
import tinder.db.sql.LikesInDB;
import tinder.db.sql.MessagesInDb;
import tinder.db.sql.UsersInDb;
import tinder.interfaces.HttpFilter;
import tinder.servlets.*;
import tinder.utils.Parser;
import tinder.utils.ResourcesOps;
import java.sql.Connection;
import java.util.EnumSet;
import java.util.Optional;

public class App {
    private static final EnumSet<DispatcherType> dispatcher = EnumSet.of(DispatcherType.REQUEST);

    public static void run() throws Exception {
        Integer port = Optional.ofNullable(System.getenv("PORT"))
                .flatMap(Parser::toInt)
                .orElse(8080);

        Server server = new Server(port);

        Database.checkAndApplyDeltas();
        Connection conn = Database.conn();
        MessagesInDb messages = new MessagesInDb(conn);
        UsersInDb users = new UsersInDb(conn);
        LikesInDB likes = new LikesInDB(conn);
        ChatsInDb chats = new ChatsInDb(conn);
        ServletContextHandler handler = new ServletContextHandler();

        String osStaticLocation = ResourcesOps.dirUnsafe("static");
        handler.addServlet(new ServletHolder(new StaticContentServlet(osStaticLocation)), "/static/*");


        Filter cookieFilter = HttpFilter.make(
                rq -> Auth.tryGetCookie(rq).isPresent(),
                rs -> rs.sendRedirect("http://localhost:8080/login")
                );

        handler.addFilter(new FilterHolder(cookieFilter), "/users", dispatcher);
        handler.addFilter(new FilterHolder(cookieFilter), "/messages", dispatcher);
        handler.addFilter(new FilterHolder(cookieFilter), "/liked", dispatcher);

        handler.addServlet(new ServletHolder(new SignInServlet(users)), "/login");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
        handler.addServlet(new ServletHolder(new SignUpServlet(conn)), "/sign-up");

        handler.addServlet(new ServletHolder(new UsersServlet(users, likes)), "/users"); // like-page
        handler.addServlet(new ServletHolder(new ChatServlet(messages, users, chats)), "/messages/*");
        handler.addServlet(new ServletHolder(new HomeServlet()), "/");
        handler.addServlet(new ServletHolder(new LikedServlet(users)), "/liked");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
