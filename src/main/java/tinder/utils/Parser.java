package tinder.utils;

import java.util.Optional;

public class Parser {
    public static Optional<Integer> toInt(String raw) {
        try {
            return Optional.of(Integer.parseInt(raw));
        } catch (Exception x) {
            return Optional.empty();
        }
    }
}
