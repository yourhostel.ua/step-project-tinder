package tinder.utils;

import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;

public class ResourcesOps {

    public static String dirUnsafe(String dir)  {
        String resource = "target/classes/";
        return resource + dir;
    }

    @SneakyThrows
    public static void redirect(HttpServletResponse resp, String to) {
        resp.sendRedirect(to);
    }
}
