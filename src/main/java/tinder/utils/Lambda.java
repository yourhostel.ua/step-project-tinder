package tinder.utils;

import tinder.interfaces.TwoParamsReturnString;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Lambda {
    static public TwoParamsReturnString<Timestamp, ZoneId> isLocalZoneId = (timestamp, zoneId) -> timestamp.toInstant().atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
}
