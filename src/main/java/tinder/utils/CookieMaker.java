package tinder.utils;

import jakarta.servlet.http.Cookie;
import java.util.UUID;

public class CookieMaker {

    public static Cookie generate() {
        return new Cookie("UID", UUID.randomUUID().toString());
    }

    public static Cookie from(UUID c) {
        return new Cookie("UID", String.valueOf(c));
    }
    public static Cookie from(String c) {
        return new Cookie("UID", c);
    }
}
