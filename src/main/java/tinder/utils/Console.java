package tinder.utils;

import java.util.Scanner;

public class Console {
    public static <T> void l(T line) {
        System.out.print(line);
    }

    public static <T> void ln(T line) {
        System.out.println(line);
    }

    public static void f(String s, Object... o) {
        System.out.printf(s, o);
    }

    public static String nextLine() {
        return new Scanner(System.in).nextLine();
    }
}
