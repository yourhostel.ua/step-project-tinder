package tinder.utils;

import java.sql.ResultSet;
import java.util.LinkedList;
import tinder.interfaces.FunctionEx;

public class DbUtils {
    public static <A> Iterable<A> convert(ResultSet rs, FunctionEx<ResultSet, A> f) throws Exception {
        LinkedList<A> as = new LinkedList<>();
        while (rs.next()) {
            A a = f.apply(rs);
            as.add(a);
        }
        return as;
    }



}
