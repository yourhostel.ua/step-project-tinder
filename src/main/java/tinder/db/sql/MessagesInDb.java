package tinder.db.sql;

import lombok.RequiredArgsConstructor;
import tinder.interfaces.MessagesDAO;
import tinder.models.Message;
import tinder.utils.DbUtils;
import java.sql.*;
import java.util.UUID;



@RequiredArgsConstructor
public class MessagesInDb implements MessagesDAO<Message> {
    private final Connection connection;

    private void insert(Message message) throws SQLException {
        String sql = """
                        INSERT INTO chat
                        (id, user_id, text, date_time)
                        VALUES (?, ?, ?, ?)
                    """;
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setString(1, String.valueOf(message.id()));
            stmt.setInt(2, message.user_id());
            stmt.setString(3, message.text());
            stmt.setTimestamp(4, message.date_time());
            stmt.execute();
        }
    }

    @Override
    public void save(Message message){
        try {
            if(message.id() != null) insert(message);
        } catch (SQLException sql_ex){
            throw new RuntimeException(sql_ex);
        }
    }

    @Override
    public Iterable<Message> getAll(UUID uuid) {
        try {
            return getAllMessages(uuid);
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }



    private Iterable<Message> getAllMessages(UUID uuid) throws Exception {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM chat
                        WHERE id = ?
                        ORDER BY date_time
                    """
        );

        st.setObject(1, String.valueOf(uuid));
        ResultSet rs0 = st.executeQuery();

        return DbUtils.convert(rs0, rs -> {
            UUID id = UUID.fromString(rs.getString("id"));
            int user_id = rs.getInt("user_id");
            String text = rs.getString("text");
            Timestamp date_time = rs.getTimestamp("date_time");
            return Message.of(id, user_id, text, date_time);
        });
    }
}
