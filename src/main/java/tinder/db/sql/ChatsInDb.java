package tinder.db.sql;
import lombok.RequiredArgsConstructor;
import tinder.interfaces.ChatsDAO;
import tinder.models.Chat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
public class ChatsInDb implements ChatsDAO<Chat> {
    private final Connection connection;

    @Override
    public void save(Chat chat) throws Exception {
        createChat(chat);
    }

    private void createChat(Chat chat) throws SQLException {
        String sql = """
                    INSERT INTO chats
                    (id)
                    VALUES (?)
                """;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, String.valueOf(chat.id()));
            stmt.execute();
        }
    }

    @Override
    public Optional<Chat> load(int userId, int convUserId) throws SQLException {
        return loadIdChat(userId, convUserId);
    }

    private Optional<Chat> loadIdChat(int userId, int convUserId) throws SQLException {
        PreparedStatement st = connection.prepareStatement(
                """   
                            SELECT id
                            FROM chat
                            WHERE id IN (
                            SELECT id
                            FROM chat
                            WHERE
                            user_id = ?
                            ) AND
                            id IN (
                            SELECT id
                            FROM chat
                            WHERE
                            user_id = ?
                            )
                        """
        );

        st.setInt(1, userId);
        st.setInt(2, convUserId);

        ResultSet rs = st.executeQuery();
        if (rs != null && rs.next()) {
            return Optional.of(Chat.of(UUID.fromString(rs.getString("id"))));
        }
        return Optional.empty();
    }

    @Override
    public Optional<Chat> load(UUID uuid) throws SQLException {
        return loadUUIDChat(uuid);
    }

    private Optional<Chat> loadUUIDChat(UUID uuid) throws SQLException {
        PreparedStatement st = connection.prepareStatement(
                """
                            SELECT *
                            FROM chats
                            WHERE id = ?
                        """
        );

        st.setObject(1, String.valueOf(uuid));

        ResultSet rs = st.executeQuery();
        if (rs != null && rs.next()) {
            return Optional.of(Chat.of(UUID.fromString(rs.getString("id"))));
        }
        return Optional.empty();
    }

}
