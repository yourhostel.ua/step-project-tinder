package tinder.db.sql;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.interfaces.LikesDAO;
import tinder.models.Like;
import tinder.utils.DbUtils;
import java.sql.*;

@RequiredArgsConstructor
public class LikesInDB implements LikesDAO<Like> {
    private final Connection connection;

    @SneakyThrows
    private void insert(Like like) {
        String sql = """
                        INSERT INTO likes
                        (from_id, to_id)
                        VALUES (?, ?)
                    """;
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setInt(1, like.fromId());
            stmt.setInt(2, like.toId());
            stmt.execute();
        }
    }

    @SneakyThrows
    private void remove(Like like) {
        String sql = """
                        DELETE FROM likes
                        WHERE from_id = ? AND to_id = ?
                    """;
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setInt(1, like.fromId());
            stmt.setInt(2, like.toId());
            stmt.execute();
        }
    }

    @Override
    public void save(Like like){
        Boolean present = likeCheck(like);
        if (like.fromId() != null && like.toId() != null && !present) insert(like);
    }

    @Override
    public void delete(Like like) {
        Boolean present = likeCheck(like);
        if(like.fromId() != null && like.toId() != null && present) remove(like);
    }
    @Override
    public Iterable<Like> getAll(Integer id) throws Exception {
        return getAllLikes(id);
    }

    private Boolean likeCheck(Like like) {
        String sql = """
                        SELECT *
                        FROM likes
                        WHERE from_id = ? AND to_id = ?
                    """;

        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setInt(1, like.fromId());
            stmt.setInt(2, like.toId());
            ResultSet rs = stmt.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Iterable<Like> getAllLikes(Integer idFrom) throws Exception {
        PreparedStatement stmt = connection.prepareStatement(
                """
                       SELECT *
                       FROM likes
                       WHERE from_id = ? (
                        SELECT id_to
                        FROM likes
                       )
                    """
        );
        stmt.setObject(1, idFrom);

        ResultSet rs0 = stmt.executeQuery();
        return DbUtils.convert(rs0, rs -> {
            int fromId = rs.getInt("from_id");
            int idTo = rs.getInt("to_id");
            return Like.of(fromId, idTo);
        });
    }
}
