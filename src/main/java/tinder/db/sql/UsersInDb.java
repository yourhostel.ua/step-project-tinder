package tinder.db.sql;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.interfaces.UserDAO;
import tinder.models.User;
import tinder.utils.DbUtils;
import java.sql.*;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
public class UsersInDb implements UserDAO<User> {
    private final Connection connection;

    @SneakyThrows
    private void insert(User user) {
        String sql = """
                        INSERT INTO users
                        (firstname, lastname, login, password, img, role, last_login, cookie)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?)
                    """;
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setString(1, user.firstname());
            stmt.setString(2, user.lastname());
            stmt.setString(3, user.login());
            stmt.setString(4, user.password());
            stmt.setString(5, user.img());
            stmt.setString(6, user.role());
            stmt.setTimestamp(7, user.last_login());
            stmt.setObject(8, String.valueOf(user.cookie()));
            stmt.execute();
        }
    }

    @Override
    public Optional<User> load(int id) {
        return findOneBy(id);
    }

    @Override
    public Optional<User> load(String login){
        return findOneBy(login);
    }

    @Override
    public Optional<User> load(String login, String password) {
        return findOneBy(login, password);
    }

    @Override
    public Optional<User> load(UUID uuid) throws Exception {
        return loadByUUID(uuid);
    }

    @Override
    public Iterable<User> getAll(UUID authUuid, Integer offset) throws Exception {
        return getAllUsers(authUuid, offset);
    }

    @Override
    public Iterable<User> getAll(Integer id) throws Exception {
        return getAllLikes(id);
    }

    @Override
    public void save(User user) {
        if (user.id() == null) insert(user);
    }

    @Override
    public void update(User user) {
        updateLastLogin(user.cookie());
    }

    @SneakyThrows
    private void updateLastLogin(UUID authUuid) {
        String sql = """
                        UPDATE users
                        SET last_login = ?
                        WHERE cookie = ?
                    """;
        try(PreparedStatement stmt = connection.prepareStatement(sql)){
            stmt.setTimestamp(1, Timestamp.from(Instant.now()));
            stmt.setString(2, String.valueOf(authUuid));
            stmt.execute();
        }
    }

    @SneakyThrows
    private Optional<User> findOneBy(String login) {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE login = ?
                    """
        );
        st.setString(1, login);

        ResultSet rs = st.executeQuery();

        if(rs.next()){
            User user = User.noCred(rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("img"),
                    rs.getString("role"),
                    rs.getTimestamp("last_login"),
                    UUID.fromString(rs.getString("cookie")));
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    @SneakyThrows
    private Optional<User> findOneBy(String login, String password) {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE login = ? AND password = ?
                    """
        );
        st.setString(1, login);
        st.setString(2, password);

        ResultSet rs = st.executeQuery();

        if(rs.next()){
            User user = User.noCred(rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("img"),
                    rs.getString("role"),
                    rs.getTimestamp("last_login"),
                    UUID.fromString(rs.getString("cookie")));
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    @SneakyThrows
    private Optional<User> findOneBy(int id) {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE id = ?
                    """
        );
        st.setInt(1, id);

        ResultSet rs = st.executeQuery();

        if(rs.next()){
            User user = User.of(rs.getInt("id"),
                    rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("img"),
                    rs.getString("role"),
                    rs.getTimestamp("last_login"),
                    UUID.fromString(rs.getString("cookie")));
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    private Optional<User> loadByUUID(UUID uuid) throws Exception {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE cookie = ?
                    """
        );
        st.setObject(1, String.valueOf(uuid));

        ResultSet rs = st.executeQuery();
        if (rs != null && rs.next()) {
            User user = User.of(rs.getInt("id"),
                    rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("img"),
                    rs.getString("role"),
                    rs.getTimestamp("last_login"),
                    UUID.fromString(rs.getString("cookie")));
            return Optional.of(user);

        }
        return Optional.empty();
    }

    private Iterable<User> getAllUsers(UUID authUuid, Integer offset) throws Exception {
        PreparedStatement st = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE cookie != ?
                        LIMIT 1
                        OFFSET ?
                    """
        );
        st.setString(1, String.valueOf(authUuid));
        st.setInt(2, offset);

        ResultSet rs0 = st.executeQuery();
        return DbUtils.convert(rs0, rs -> {
            String  role = rs.getString("lastname");
            int id = rs.getInt("id");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String img = rs.getString("img");
            UUID cookie = UUID.fromString(rs.getString("cookie"));
            return User.of(id, firstname, lastname, img, role, rs.getTimestamp("last_login"), cookie);
        });
    }

    private Iterable<User> getAllLikes(Integer idFrom) throws Exception {
        PreparedStatement stmt = connection.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE users.id IN (
                            SELECT to_id
                            FROM likes
                            WHERE from_id = ?
                            )
                    """
        );
        stmt.setObject(1, idFrom);

        ResultSet rs0 = stmt.executeQuery();
        return DbUtils.convert(rs0, rs -> {
            int id = rs.getInt("id");
            UUID cookie = UUID.fromString(rs.getString("cookie"));
            String  firstName = rs.getString("firstname");
            String  lastName = rs.getString("lastname");
            String  role = rs.getString("role");
            String  img = rs.getString("img");
            Timestamp lastLogin = rs.getTimestamp("last_login");
            return User.of(id, firstName, lastName, img, role, lastLogin, cookie);
        });
    }
}
