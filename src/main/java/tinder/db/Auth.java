package tinder.db;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class Auth {
    public static String CookieName = "UID";
    private static final Cookie[] cookie = new Cookie[0];

    public static Optional<UUID> tryGetCookie(Cookie[] c0){
        return Arrays.stream(c0 != null ? c0 : cookie)
                .filter(a -> a.getName().equals(CookieName))
                .findAny()
                .map(x -> UUID.fromString(x.getValue()));
    }

    public static Optional<UUID> tryGetCookie(HttpServletRequest req){
        Cookie[] cookies = req.getCookies();
        return tryGetCookie(cookies);
    }

    public static UUID getCookieOrThrow(HttpServletRequest rq) {
        return tryGetCookie(rq).orElseThrow(() -> new RuntimeException("Cookie is absent"));
    }
}
