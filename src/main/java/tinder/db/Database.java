package tinder.db;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    private static final String url = System.getenv("URL_DB");
    private static final String user = System.getenv("USER_DB");
    private static final String pass = System.getenv("PASSWORD_DB");


    /** analyze current structure & apply deltas */
    public static void checkAndApplyDeltas() {
        FluentConfiguration conf = new FluentConfiguration()
                .dataSource(url, user, pass);
        Flyway flyway = new Flyway(conf);
        flyway.migrate();

    }

    public static Connection conn() throws SQLException {
        return DriverManager.getConnection(url, user, pass);
    }

}
