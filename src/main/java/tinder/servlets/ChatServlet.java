package tinder.servlets;

import freemarker.template.Configuration;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import tinder.db.Auth;
import tinder.db.sql.ChatsInDb;
import tinder.db.sql.MessagesInDb;
import tinder.db.sql.UsersInDb;
import tinder.models.Chat;
import tinder.models.MassageConvertTime;
import tinder.models.Message;
import tinder.models.User;
import tinder.utils.ConfigFM;


import java.io.PrintWriter;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static tinder.utils.Console.f;
import static tinder.utils.Lambda.isLocalZoneId;
import static tinder.utils.Parser.toInt;
import static tinder.utils.ResourcesOps.redirect;

@RequiredArgsConstructor
public class ChatServlet extends HttpServlet {
    private final MessagesInDb messages;
    private final UsersInDb users;
    private final ChatsInDb chats;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        UUID uid = Auth.getCookieOrThrow(req), uidChat;
        User convUser = null, user = null;
        int idConvUser;

        Optional<Integer> idConvUserOpt = toInt(req.getPathInfo().substring(1));

        if (idConvUserOpt.isPresent()) {
            idConvUser = idConvUserOpt.get();
        } else {
            redirect(resp, "/liked");
            return;
        }

        try {
            Optional<User> convUserOpt = users.load(idConvUser);
            Optional<User> userOpt = users.load(uid);
            if (userOpt.isPresent() && convUserOpt.isPresent()) {
                convUser = convUserOpt.get();
                user = userOpt.get();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            uidChat = chats.load(user.id(), idConvUser)
                    .orElseThrow(() -> new RuntimeException("There is no such chat."))
                    .id();
        } catch (Exception e) {
            f("%s A new chat has been created\n", e);
            try {
                UUID uuid = UUID.randomUUID();

                chats.save(Chat.of(uuid));

                uidChat = chats.load(uuid)
                        .orElseThrow(() -> new RuntimeException("Creation error UUID"))
                        .id();

                messages.save(Message.of(uidChat,
                        users.load(uid).orElseThrow(() -> new RuntimeException("error id User")).id(),
                        new Timestamp(System.currentTimeMillis())));

                messages.save(Message.of(uidChat,
                        idConvUser,
                        new Timestamp(System.currentTimeMillis())));

            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        Cookie chatUUID = new Cookie("chatUUID", uidChat.toString());
        Cookie idConUser = new Cookie("idConvUser", Integer.toString(idConvUser));
        resp.addCookie(chatUUID);
        resp.addCookie(idConUser);

        String timeZone = tryGetCookie(req.getCookies(), "timeZone").orElseThrow(() -> new RuntimeException("error cookie timeZone in doGet ChatServlet"));
        ZoneId zoneId = ZoneId.of(String.valueOf(timeZone));

        HashMap<String, Object> data = new HashMap<>();
        try (PrintWriter w = resp.getWriter()) {
            Configuration cfg = ConfigFM.make("tpl");
            List<MassageConvertTime> mess = StreamSupport
                    .stream(messages.getAll(uidChat).spliterator(), false)
                    .map(m -> MassageConvertTime.of(
                            m.id(),
                            m.user_id(),
                            m.text(),
                            isLocalZoneId.get(m.date_time(),zoneId)))
                    .collect(Collectors.toList());

            data.put("convImg", convUser.img());
            data.put("convUser", convUser.firstname() + " " + convUser.lastname());
            data.put("convUserFirstname", convUser.firstname());
            data.put("img", user.img());
            data.put("items", mess);
            data.put("user_id", user.id());
            data.put("buttonA", "");
            data.put("buttonB", "<a class=\"line\" href=\"/logout\">Logout</a>");
            data.put("tplMenu", cfg.getTemplate("tplMenu.ftl"));
            cfg.getTemplate("chat.ftl").process(data, w);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        UUID uid = Auth.getCookieOrThrow(req);
        Cookie[] cookies = req.getCookies();

        UUID uidChat = UUID.fromString(tryGetCookie(cookies, "chatUUID").orElseThrow(() -> new RuntimeException("error cookie chatUUID in doPost ChatServlet")));
        String idConvUser = tryGetCookie(cookies, "idConvUser").orElseThrow(() -> new RuntimeException("error cookie idConvUser in doPost ChatServlet"));

        Cookie b = new Cookie("chatUUID", "");
        b.setMaxAge(0);
        Cookie c = new Cookie("idConvUser", "");
        c.setMaxAge(0);
        resp.addCookie(b);
        resp.addCookie(c);
        try {
            if (users.load(uid).isPresent()) {
                messages.save(Message.of(uidChat,
                        users.load(uid).get().id(),
                        req.getParameter("text"),
                        new Timestamp(System.currentTimeMillis())));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        redirect(resp, String.format("/messages/%s", idConvUser));
    }

    private Optional<String> tryGetCookie(Cookie[] c0, String nameCookie) {
        return Arrays.stream(c0 != null ? c0 : new Cookie[0])
                .filter(a -> a.getName().equals(nameCookie))
                .findAny()
                .map(Cookie::getValue);
    }
}
