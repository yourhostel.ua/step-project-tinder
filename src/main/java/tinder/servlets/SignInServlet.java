package tinder.servlets;

import freemarker.template.Configuration;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.db.Auth;
import tinder.db.sql.UsersInDb;
import tinder.models.User;
import tinder.utils.ConfigFM;
import tinder.utils.CookieMaker;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Optional;

@RequiredArgsConstructor
public class SignInServlet extends HttpServlet {

    private final UsersInDb users;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        Configuration cfg = ConfigFM.make("tpl");
        String tplMenu = "",
                buttonA = "<a class=\"line\" href=\"/sign-up\">Sign up</a>",
                buttonB = "";

        if (Auth.tryGetCookie(req).isPresent()){
            tplMenu = String.valueOf(cfg.getTemplate("tplMenu.ftl"));
            buttonA = "";
            buttonB = "<a class=\"line\" href=\"/logout\">Logout</a>";
        }

        HashMap<String, Object> data = new HashMap<>();
        try (PrintWriter w = resp.getWriter()) {
            data.put("signIn", cfg
                    .getTemplate("sign-in.ftl"));
            data.put("signUp", "");
            data.put("additionalInfo", "");
            data.put("buttonA", buttonA);
            data.put("buttonB", buttonB);
            data.put("tplMenu", tplMenu);

            cfg.getTemplate("login.ftl").process(data, w);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Optional<User> founded = users.load(login, password);

        if(founded.isPresent()){
            User user = founded.get();
            Cookie c0 = CookieMaker.from(user.cookie());
//            users.updateLastLogin(user.cookie());
            users.update(user);
            resp.addCookie(c0);
            resp.sendRedirect("/users");
        } else {
            HashMap<String, Object> data = new HashMap<>();
            try (PrintWriter w = resp.getWriter()) {
                Configuration cfg = ConfigFM.make("tpl");
                String tplMenu = "",
                        buttonA = "<a class=\"line\" href=\"/sign-up\">Sign up</a>",
                        buttonB = "";

                if (Auth.tryGetCookie(req).isPresent()){
                    tplMenu = String.valueOf(cfg.getTemplate("tplMenu.ftl"));
                    buttonA = "";
                    buttonB = "<a class=\"line\" href=\"/logout\">Logout</a>";
                }

                data.put("signIn", cfg
                        .getTemplate("sign-in.ftl"));
                data.put("signUp", "");
                data.put("additionalInfo", "User not found");
                data.put("buttonA", buttonA);
                data.put("buttonB", buttonB);
                data.put("tplMenu", tplMenu);

                cfg.getTemplate("login.ftl").process(data, w);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
