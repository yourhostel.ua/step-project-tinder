package tinder.servlets;

import freemarker.template.Configuration;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.db.Auth;
import tinder.db.sql.LikesInDB;
import tinder.db.sql.UsersInDb;
import tinder.models.Like;
import tinder.models.User;
import tinder.utils.ConfigFM;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@RequiredArgsConstructor
public class UsersServlet extends HttpServlet {
    private final UsersInDb users;
    private final LikesInDB likes;
    private static Integer offset = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Configuration cfg = ConfigFM.make("tpl");

        Optional<UUID> userCookie = Auth.tryGetCookie(req);
        HashMap<String, Object> data = new HashMap<>();

        try (PrintWriter w = resp.getWriter()){
            if (userCookie.isPresent()) {
                Iterable<User> all = users.getAll(userCookie.get(), offset);
                if(all.iterator().hasNext()) {
                    data.put("users", all);
                    data.put("buttonA", "");
                    data.put("buttonB", "<a class=\"line\" href=\"/logout\">Logout</a>");
                    data.put("tplMenu", cfg.getTemplate("tplMenu.ftl"));
                    cfg.getTemplate("like-page.ftl").process(data, w);
                } else {
                    offset = 0;
                    resp.sendRedirect("/liked");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        UUID uuid = Auth.getCookieOrThrow(req);
        Integer idFrom = users.load(uuid).get().id();
        Integer idTo = Integer.valueOf(req.getParameter("idTo"));
        String like = req.getParameter("like");
        Like l = new Like(idFrom, idTo);

        if(like.equals("like")) {
            likes.save(l);
        } else {
            likes.delete(l);
        }
            offset++;
            resp.sendRedirect("/users");
    }
}
