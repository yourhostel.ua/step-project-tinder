package tinder.servlets;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static tinder.utils.ResourcesOps.redirect;


public class LogoutServlet extends HttpServlet {

    // http://localhost:8080/logout
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Cookie b = new Cookie("chatUUID", "");b.setMaxAge(0);
        Cookie c = new Cookie("idConvUser", "");c.setMaxAge(0);
        Cookie f = new Cookie("UID", "");f.setMaxAge(0);

        resp.addCookie(b);
        resp.addCookie(c);
        resp.addCookie(f);
        redirect(resp, "/");
    }
}
