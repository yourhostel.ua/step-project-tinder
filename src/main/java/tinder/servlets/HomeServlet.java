package tinder.servlets;

import freemarker.template.Configuration;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletResponse;
import tinder.db.Auth;
import tinder.utils.ConfigFM;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        Configuration cfg = ConfigFM.make("tpl");
        String tplMenu = "",
                buttonA = "<a class=\"line\" href=\"/sign-up\">Sign up</a>",
                buttonB = "<a class=\"line\" href=\"/login\">Login</a>";

        if (Auth.tryGetCookie(req).isPresent()){
            tplMenu = String.valueOf(cfg.getTemplate("tplMenu.ftl"));
            buttonA = "";
            buttonB = "<a class=\"line\" href=\"/logout\">Logout</a>";
        }

        HashMap<String, Object> data = new HashMap<>();
        try (PrintWriter w = resp.getWriter()) {

            data.put("buttonA", buttonA);
            data.put("buttonB", buttonB);
            data.put("tplMenu", tplMenu);

            cfg.getTemplate("home.ftl").process(data, w);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
