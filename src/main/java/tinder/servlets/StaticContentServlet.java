package tinder.servlets;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class StaticContentServlet extends HttpServlet {
    private final String osStaticLocation;

    public StaticContentServlet(String osStaticLocation) {
        this.osStaticLocation = osStaticLocation;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String pathInfo = req.getPathInfo();

        Path file = Path.of(osStaticLocation, pathInfo);

        if (!file.toFile().exists()) {
            resp.setStatus(404);
        } else {
            try (ServletOutputStream os = resp.getOutputStream()) {
                Files.copy(file, os);
            }
        }
    }
}
