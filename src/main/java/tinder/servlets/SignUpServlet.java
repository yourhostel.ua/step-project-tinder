package tinder.servlets;

import freemarker.template.Configuration;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.db.Auth;
import tinder.db.sql.UsersInDb;
import tinder.models.User;
import tinder.utils.ConfigFM;
import tinder.utils.CookieMaker;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import jakarta.servlet.http.Cookie;
@RequiredArgsConstructor
public class SignUpServlet extends HttpServlet {

    private final Connection connection;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        Configuration cfg = ConfigFM.make("tpl");
        String tplMenu = "",
                buttonA = "",
                buttonB = "<a class=\"line\" href=\"/login\">Sign in</a>";

        if (Auth.tryGetCookie(req).isPresent()){
            tplMenu = String.valueOf(cfg.getTemplate("tplMenu.ftl"));
            buttonA = "";
            buttonB = "<a class=\"line\" href=\"/logout\">Logout</a>";
        }

        HashMap<String, Object> data = new HashMap<>();
        try (PrintWriter w = resp.getWriter()) {
            data.put("signUp", cfg
                    .getTemplate("sign-up.ftl"));
            data.put("signIn", "");
            data.put("signUpButton", "");
            data.put("additionalInfo", "");
            data.put("buttonA", buttonA);
            data.put("buttonB", buttonB);
            data.put("tplMenu", tplMenu);

            cfg.getTemplate("login.ftl").process(data, w);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        Configuration cfg = ConfigFM.make("tpl");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String role = req.getParameter("role");
        String img = req.getParameter("img");
        
        UsersInDb userDAO = new UsersInDb(connection);

        Optional<User> existingUser = userDAO.load(login);


        if(existingUser.isPresent()){
            String tplMenu = "",
                    buttonA = "",
                    buttonB = "<a class=\"line\" href=\"/login\">Sign in</a>";

            if (Auth.tryGetCookie(req).isPresent()){
                tplMenu = String.valueOf(cfg.getTemplate("tplMenu.ftl"));
                buttonA = "";
                buttonB = "<a class=\"line\" href=\"/logout\">Logout</a>";
            }

            HashMap<String, Object> data = new HashMap<>();
            try (PrintWriter w = resp.getWriter()) {
                data.put("signUp", cfg
                        .getTemplate("sign-up.ftl"));
                data.put("signIn", "");
                data.put("signUpButton", "");
                data.put("additionalInfo", "Please use another login");
                data.put("buttonA", buttonA);
                data.put("buttonB", buttonB);
                data.put("tplMenu", tplMenu);

                cfg.getTemplate("login.ftl").process(data, w);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            Cookie cookie = CookieMaker.generate();
            String cv = cookie.getValue();
            UUID uuid = UUID.fromString(cv);
            User user = User.noId(firstName, lastName, login, password, img, role, Timestamp.from(Instant.now()) ,uuid);
            userDAO.save(user);
            resp.addCookie(cookie);
            resp.sendRedirect("/users");
        }

    }
}
