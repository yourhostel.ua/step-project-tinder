package tinder.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import tinder.db.Auth;
import tinder.db.sql.UsersInDb;
import tinder.utils.ConfigFM;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.UUID;

@RequiredArgsConstructor
public class LikedServlet extends HttpServlet {

    private final UsersInDb users;

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        Configuration cfg = ConfigFM.make("tpl");

        UUID uuid = Auth.getCookieOrThrow(req);
        Integer idFrom = users.load(uuid).get().id();
        HashMap<String, Object> data = new HashMap<>();

        try (PrintWriter w = resp.getWriter()) {
            data.put("likes", users.getAll(idFrom));
            data.put("buttonA", "");
            data.put("buttonB", "<a class=\"line\" href=\"/logout\">Logout</a>");
            data.put("tplMenu", cfg.getTemplate("tplMenu.ftl"));

            cfg
                .getTemplate("people-list.ftl")
                .process(data, w);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }
}
