package tinder.models;

import java.util.UUID;

public record MassageConvertTime (UUID id, Integer user_id, String text, String date_time){
        public static MassageConvertTime of(UUID id, Integer user_id, String text, String date_time) {
            return new MassageConvertTime(id, user_id, text, date_time);
        }
}
