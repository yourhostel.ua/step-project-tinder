package tinder.models;
import java.util.UUID;

public record Chat(UUID id){
    public static Chat of(UUID id){
        return new Chat(id);
    }
}
