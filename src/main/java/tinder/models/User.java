package tinder.models;

import tinder.interfaces.Identifable;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public record User(Integer id, String firstname, String lastname, String login, String password, String img, String role, Timestamp last_login, UUID cookie) implements Identifable {
    public static User noId(String firstname, String lastname, String login, String password, String img, String role, Timestamp last_login, UUID cookie){
        return new User(null, firstname, lastname, login, password,img, role, last_login, cookie);
    }
    public static User noCred(String firstname, String lastname, String img, String role, Timestamp last_login, UUID cookie){
        return new User(null, firstname, lastname, null, null, img, role, last_login, cookie);
    }
    public static User of(Integer id, String firstname, String lastname, String img, String role, Timestamp last_login, UUID cookie){
        return new User(id, firstname, lastname, null, null, img, role, last_login, cookie);
    }

    public String getLastLogin() {
        return this.last_login().toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    public String getDaysOfLogin() {
        long now = System.currentTimeMillis();
        long lastLoginMilly = this.last_login.getTime();
        long diffInMilly = now - lastLoginMilly;
        long diff = TimeUnit.DAYS.convert(diffInMilly, TimeUnit.MILLISECONDS);
        if(diff == 0) return "recently";
        return String.format("%d days ago", diff);
    }
}
