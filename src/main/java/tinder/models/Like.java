package tinder.models;

public record Like(
        Integer fromId,
        Integer toId
) {
    public static Like of(Integer fromId, Integer toId) { return new Like(fromId, toId); }
}
