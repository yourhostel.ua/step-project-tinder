package tinder.models;

import java.sql.Timestamp;
import java.util.UUID;

public record Message(UUID id, Integer user_id, String text, Timestamp date_time) {
    public static Message of(UUID id, Integer user_id, String text, Timestamp date_time){
        return new Message(id, user_id, text, date_time);
    }
    public static Message of(UUID id, Integer user_id, Timestamp date_time){
        return new Message(id, user_id, null, date_time);
    }
}
