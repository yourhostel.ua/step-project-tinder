package tinder.interfaces;

public interface LikesDAO<A> extends DAO<A> {
    void delete(A a) throws Exception;

    Iterable<A> getAll(Integer id) throws Exception;
}
