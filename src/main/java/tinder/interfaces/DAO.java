package tinder.interfaces;

public interface DAO<A>  {
    void save(A a) throws Exception;

}
