package tinder.interfaces;

import java.util.UUID;

public interface MessagesDAO<A> extends DAO<A> {
    Iterable<A> getAll(UUID uuid) throws Exception;
}
