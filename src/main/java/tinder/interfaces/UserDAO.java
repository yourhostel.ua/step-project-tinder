package tinder.interfaces;

import java.util.Optional;
import java.util.UUID;

public interface UserDAO<A> extends DAO<A> {

    void update(A a) throws Exception;
    Optional<A> load(int id) throws Exception;
    Optional<A> load(String login) throws Exception;
    Optional<A> load(UUID uuid) throws Exception;
    Iterable<A> getAll(UUID uuid, Integer offset) throws Exception;
    Iterable<A> getAll(Integer id) throws Exception;
    Optional<A> load(String login, String password) throws Exception;

}
