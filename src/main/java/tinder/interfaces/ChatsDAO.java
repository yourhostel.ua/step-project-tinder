package tinder.interfaces;

import java.util.Optional;
import java.util.UUID;

public interface ChatsDAO<A> extends DAO<A> {
    Optional<A> load(UUID uuid) throws Exception;
    Optional<A> load(int id, int id0) throws Exception;

}
