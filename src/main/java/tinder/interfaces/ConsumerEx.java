package tinder.interfaces;

import java.util.function.Consumer;

public interface ConsumerEx<A> extends Consumer<A> {

    void acceptEx(A a) throws Exception;

    @Override
    default void accept(A a) {
        try {
            acceptEx(a);
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

}
