package tinder.interfaces;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;

import java.util.function.Function;

public interface HttpFilter extends Filter {

    private boolean isHttp(ServletRequest rq0, ServletResponse resp0){
        return rq0 instanceof HttpServletRequest && resp0 instanceof HttpServletResponse;
    }
    @SneakyThrows
    void doHttpFilter(HttpServletRequest request,
                      HttpServletResponse response,
                      FilterChain chain
    );

    @SneakyThrows
    @Override
    default void init(FilterConfig filterConfig) {}
    @SneakyThrows
    @Override
    default void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)  {
        if(isHttp(servletRequest, servletResponse)){
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            doHttpFilter(request, response, filterChain);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
    @Override
    default void destroy() {}

    static HttpFilter make(
            Function<HttpServletRequest, Boolean> checkFn,
            ConsumerEx<HttpServletResponse> failedFn
    ) {

        return new HttpFilter() {
            @SneakyThrows
            @Override
            public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
                if (checkFn.apply(request)) {
                    chain.doFilter(request, response);
                } else {
                    failedFn.accept(response);
                }
            }
        };

    }
}
