package tinder.interfaces;

public interface TwoParamsReturnString<T,Z> {
        String get(T a, Z b);
}
